package com.ushakov.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.ushakov.tm.command.AbstractDataCommand;
import com.ushakov.tm.dto.Domain;
import com.ushakov.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String description() {
        return "Load data from xml by Faster XML.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    public @NotNull String name() {
        return "data-load-xml-fasterxml";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}

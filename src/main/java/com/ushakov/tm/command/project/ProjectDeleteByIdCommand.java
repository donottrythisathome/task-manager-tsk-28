package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectDeleteByIdCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Delete project with child tasks.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER PROJECT ID");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectTaskService().deleteProjectById(userId, projectId);
    }

    @Override
    @NotNull
    public String name() {
        return "delete-project-with-tasks";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}

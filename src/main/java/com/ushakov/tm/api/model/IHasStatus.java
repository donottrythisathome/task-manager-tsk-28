package com.ushakov.tm.api.model;

import com.ushakov.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}

package com.ushakov.tm.bootstrap;

import com.ushakov.tm.api.repository.ICommandRepository;
import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.api.repository.ITaskRepository;
import com.ushakov.tm.api.repository.IUserRepository;
import com.ushakov.tm.api.service.*;
import com.ushakov.tm.command.AbstractCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.system.UnknownCommandException;
import com.ushakov.tm.repository.CommandRepository;
import com.ushakov.tm.repository.ProjectRepository;
import com.ushakov.tm.repository.TaskRepository;
import com.ushakov.tm.repository.UserRepository;
import com.ushakov.tm.service.*;
import com.ushakov.tm.util.SystemUtil;
import com.ushakov.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.Optional;
import java.util.Set;

public class  Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @SneakyThrows
    public void initCommands() {
        @NotNull final Reflections reflections = new Reflections("com.ushakov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(com.ushakov.tm.command.AbstractCommand.class);
        for(@NotNull final Class<? extends AbstractCommand> clazz :classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            try{
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e){
                loggerService.error(e);
            }
        }
    }

    private void initUsers() {
        userService.add("user", "user", "try@mail.ru");
        userService.add("admin", "admin", Role.ADMIN);
    }

    private void initData() {
        projectService.add(userService.findOneByLogin("user").getId(), "Project1", "First Project");
        projectService.add(userService.findOneByLogin("admin").getId(), "Project2", "Second Project");
        taskService.add(userService.findOneByLogin("user").getId(), "MyFirstTask", "Sample");
        taskService.add(userService.findOneByLogin("admin").getId(), "AnotherTask", "Another One");
    }

    private void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent() || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseCommand(@Nullable final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent() || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void run(@Nullable final String... args) {
        loggerService.debug("EXAMPLE");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        initData();
        while (true) {
            System.out.println("ENTER COMMAND");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[SUCCESS]\n");
            } catch (Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]\n");
            }
        }
    }

}

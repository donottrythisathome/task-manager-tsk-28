package com.ushakov.tm.exception.empty;

import com.ushakov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Id is empty");
    }

}
